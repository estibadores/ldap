FROM registry.sindominio.net/debian

COPY debconf-selections /etc/debconf-slapd-selections
RUN debconf-set-selections /etc/debconf-slapd-selections

RUN apt-get update && \
    apt-get install -y --no-install-recommends slapd && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN slapcat -n 0 \
    |sed 's/cn: config/cn: config\nolcPasswordHash: {CRYPT}\nolcPasswordCryptSaltFormat: $6$%.16s/' \
    |sed 's/cn: module{0}/cn: module{0}\nolcModuleLoad: {0}lastbind/' > /tmp/config.ldif && \
    rm -rf /etc/ldap/slapd.d/* && \
    slapadd -n 0 -F /etc/ldap/slapd.d/ -l /tmp/config.ldif && \
    rm /tmp/config.ldif

RUN echo "dn: olcOverlay={0}lastbind, olcDatabase={1}mdb,cn=config\nobjectClass: olcLastBindConfig\nolcOverlay: {0}lastbind" \
    | slapadd -n 0

COPY *.schema *.ldif /etc/ldap/schema/
RUN slapadd -n 0 -l /etc/ldap/schema/sindominio.ldif
RUN slapadd -n 0 -l /etc/ldap/schema/openpgp.ldif

RUN chmod -R o+rw /etc/ldap/slapd.d/ && \
    chmod o+rwx /etc/ldap/slapd.d/cn=config/ && \
    chmod o+rwx /etc/ldap/slapd.d/cn=config/cn=schema/ && \
    chmod o+rw /var/run/slapd/

VOLUME ["/var/lib/ldap"]

ENTRYPOINT ["/usr/sbin/slapd"]
CMD ["-h", "ldap://0.0.0.0:3389/", "-dNone"]
